package api

import (
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	_ "gitlab.com/market/mk_go_api_gateway/api/docs"
	handlers "gitlab.com/market/mk_go_api_gateway/api/handler"
	"gitlab.com/market/mk_go_api_gateway/config"
)

func SetUpAPI(r *gin.Engine, h *handlers.Handler, cfg config.Config) {
	// docs.SwaggerInfo.Title = cfg.ServiceName
	// docs.SwaggerInfo.Version = cfg.Version
	// docs.SwaggerInfo.Schemes = []string{cfg.HTTPScheme}

	// @securityDefinitions.apikey ApiKeyAuth
	// @in header
	// @name Authorization

	r.Use(customCORSMiddleware())
	r.Use(MaxAllowed(5000))

	// register
	r.POST("/register", h.RegisterStaff)

	// login
	r.POST("/login", h.LoginStaff)

	//logout
	r.POST("/logout", h.LogOutStaff)

	v1 := r.Group("/v1")


	v1.Use(h.AuthMiddleware())

	// Content Service Branch Api
	v1.POST("branch", h.CreateBranch)
	v1.GET("branch/:id", h.GetBranchById)
	v1.GET("branch", h.GetBranchList)
	v1.PUT("branch/:id", h.UpdateBranch)
	v1.DELETE("branch/:id", h.DeleteBranch)

	// Content Service Market Api
	v1.POST("market", h.CreateMarket)
	v1.GET("market/:id", h.GetMarketById)
	v1.GET("market", h.GetMarketList)
	v1.PUT("market/:id", h.UpdateMarket)
	v1.DELETE("market/:id", h.DeleteMarket)

	// Content Service Diller Api
	v1.POST("diller", h.CreateDiller)
	v1.GET("diller/:id", h.GetDillerById)
	v1.GET("diller", h.GetDillerList)
	v1.PUT("diller/:id", h.UpdateDiller)
	v1.DELETE("diller/:id", h.DeleteDiller)

	// Content Service Staff Api
	v1.POST("staff", h.CreateStaff)
	v1.GET("staff/:id", h.GetStaffById)
	v1.GET("staff", h.GetStaffList)
	v1.PUT("staff/:id", h.UpdateStaff)
	v1.DELETE("staff/:id", h.DeleteStaff)

	// Content Service Product Api
	v1.POST("product", h.CreateProduct)
	v1.GET("product/:id", h.GetProductById)
	v1.GET("product", h.GetProductList)
	v1.PUT("product/:id", h.UpdateProduct)
	v1.DELETE("product/:id", h.DeleteProduct)

	// Content Service Brand Api
	v1.POST("brand", h.CreateBrand)
	v1.GET("brand/:id", h.GetBrandById)
	v1.GET("brand", h.GetBrandList)
	v1.PUT("brand/:id", h.UpdateBrand)
	v1.DELETE("brand/:id", h.DeleteBrand)

	// Content Service Category Api
	v1.POST("category", h.CreateCategory)
	v1.GET("category/:id", h.GetCategoryById)
	v1.GET("category", h.GetCategoryList)
	v1.PUT("category/:id", h.UpdateCategory)
	v1.DELETE("category/:id", h.DeleteCategory)

	// Content Service Coming Api
	v1.POST("coming", h.CreateComing)
	v1.GET("coming/:id", h.GetComingById)
	v1.GET("coming", h.GetComingList)
	v1.PUT("coming/:id", h.UpdateComing)
	v1.DELETE("coming/:id", h.DeleteComing)

	// Content Service ComingProducts Api
	v1.POST("coming_product", h.CreateComingProducts)
	v1.GET("coming_product/:id", h.GetComingProductsById)
	v1.GET("coming_product", h.GetComingProductsList)
	v1.PUT("coming_product/:id", h.UpdateComingProducts)
	v1.DELETE("coming_product/:id", h.DeleteComingProducts)

	// Content Service Remainder Api
	v1.POST("remainder", h.CreateRemainder)
	v1.GET("remainder/:id", h.GetRemainderById)
	v1.GET("remainder", h.GetRemainderList)
	v1.PUT("remainder/:id", h.UpdateRemainder)
	v1.DELETE("remainder/:id", h.DeleteRemainder)

	// Content Service Sale Api
	v1.POST("sale", h.CreateSale)
	v1.GET("sale/:id", h.GetSaleById)
	v1.GET("sale", h.GetSaleList)
	v1.PUT("sale/:id", h.UpdateSale)
	v1.DELETE("sale/:id", h.DeleteSale)

	// Content Service SaleProduct Api
	v1.POST("sale_product", h.CreateSaleProduct)
	v1.GET("sale_product/:id", h.GetSaleProductById)
	v1.GET("sale_product", h.GetSaleProductList)
	v1.PUT("sale_product/:id", h.UpdateSaleProduct)
	v1.DELETE("sale_product/:id", h.DeleteSaleProduct)

	// Content Service Payment Api
	v1.POST("payment", h.CreatePayment)
	v1.GET("payment/:id", h.GetPaymentById)
	v1.GET("payment", h.GetPaymentList)
	v1.PUT("payment/:id", h.UpdatePayment)
	v1.DELETE("payment/:id", h.DeletePayment)

	// Content Service Shift Api
	v1.POST("shift", h.CreateShift)
	v1.GET("shift/:id", h.GetShiftById)
	v1.GET("shift", h.GetShiftList)
	v1.PUT("shift/:id", h.UpdateShift)
	v1.DELETE("shift/:id", h.DeleteShift)

	// Content Service ShiftTransaction Api
	v1.POST("shift_transaction", h.CreateShiftTransaction)
	v1.GET("shift_transaction/:id", h.GetShiftTransactionById)
	v1.GET("shift_transaction", h.GetShiftTransactionList)
	v1.PUT("shift_transaction/:id", h.UpdateShiftTransaction)
	v1.DELETE("shift_transaction/:id", h.DeleteShiftTransaction)

	//  Send Prixod

	v1.GET("do-income/:coming_id", h.Send_Coming)

	//  Open Shift

	v1.POST("open_shift", h.OpenShift)

	// Close Shift

	v1.GET("close_shift/:id", h.CloseShift)

	// Scan Barcode

	v1.GET("scan_barcode", h.ScanBarcode)

	//  Do Sale Product

	v1.POST("do_sale_product", h.DoSaleProduct)

	//  Do Sale

	v1.GET("do_sale/:sale_id", h.DoSale)

	url := ginSwagger.URL("swagger/doc.json") // The url pointing to API definition
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

}

func MaxAllowed(n int) gin.HandlerFunc {
	var countReq int64
	sem := make(chan struct{}, n)
	acquire := func() {
		sem <- struct{}{}
		countReq++
	}

	release := func() {
		select {
		case <-sem:
		default:
		}
		countReq--
	}

	return func(c *gin.Context) {
		acquire()       // before request
		defer release() // after request

		c.Set("sem", sem)
		c.Set("count_request", countReq)

		c.Next()
	}
}

func customCORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, PATCH, DELETE, HEAD")
		c.Header("Access-Control-Allow-Headers", "Platform-Id, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Max-Age", "3600")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
