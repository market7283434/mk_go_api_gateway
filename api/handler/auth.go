package handlers

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/content_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// Create Staff godoc
// @ID register_Staff
// @Router /register [POST]
// @Summary Register
// @Description Register
// @Tags Register
// @Accept json
// @Produce json
// @Param Staff body content_service.CreateStaff true "CreateStaffRequest"
// @Success 200 {object} http.Response{data=object{}} "Success Request"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) RegisterStaff(c *gin.Context) {

	var createStaff content_service.CreateStaff

	err := c.ShouldBindJSON(&createStaff) // parse req body to given type struct
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "register Staff",
			"message": err.Error(),
		})
		return
	}
	if len(createStaff.Login) < 6 || len(createStaff.Password) < 6 {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "register Staff",
			"message": "Login and Password length must be longer than 6",
		})
		return
	}



	resp, err := h.services.StaffService().GetByID(context.Background(), &content_service.StaffPrimaryKey{
		Login: createStaff.Login,
	})
	
	if err == nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "register Staff",
			"message": "Staff already exists",
		})
		return
	} else {
		if err.Error() == "rpc error: code = InvalidArgument desc = no rows in result set" {
		} else {
			c.JSON(http.StatusBadRequest, map[string]interface{}{
				"status":  "register Staff",
				"message": err.Error(),
			})
			return
		}
	}

	id, err := h.services.StaffService().Create(context.Background(), &createStaff)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "storage.Staff.register",
			"message": err.Error(),
		})
		return
	}

	resp, err = h.services.StaffService().GetByID(context.Background(), &content_service.StaffPrimaryKey{Id: id.Id})
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "storage.Staff.getByID",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusCreated, resp)
}

// Login godoc
// @ID login_Staff
// @Router /login [POST]
// @Summary Login
// @Description Login
// @Tags Login
// @Accept json
// @Produce json
// @Param Staff body content_service.StaffLogin true "LoginRequest"
// @Success 200 {object} http.Response{data=object{}} "Success Request"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) LoginStaff(c *gin.Context) {

	var logPass content_service.StaffLogin

	err := c.ShouldBindJSON(&logPass) // parse req body to given type struct
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "login Staff",
			"message": err.Error(),
		})
		return
	}

	if len(logPass.Login) < 6 || len(logPass.Password) < 6 {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "login Staff",
			"message": "Login and Password length must be longer than 6",
		})
		return
	}

	resp, err := h.services.StaffService().GetByID(context.Background(), &content_service.StaffPrimaryKey{
		Login: logPass.Login,
	})
	if err != nil {
		if err.Error() == "no rows in result set" {
			c.JSON(http.StatusBadRequest, map[string]interface{}{
				"status":  "login Staff",
				"message": "this staff does not exist",
			})
			return
		} else {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "login Staff",
				"message": err.Error(),
			})
			return
		}

	}

	m := make(map[string]interface{})

	m["user_id"] = resp.Id

	token, err := helper.GenerateJWT(m, time.Hour, "Don't Have Any Secret Key")

	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "token response",
			"message": err.Error(),
		})

		return
	}

	c.SetCookie("token", token, 60*60*24, "/", "localhost", false, true)
	c.JSON(http.StatusCreated, nil)
}

// LogOut godoc
// @ID logout_Staff
// @Router /logout [POST]
// @Summary LogOut
// @Description Login
// @Tags LogOut
// @Accept json
// @Produce json
// @Success 200 {object} http.Response{data=object{}} "Success Request"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) LogOutStaff(c *gin.Context) {
	value, err := c.Cookie("token")
	if err != nil {
		c.String(http.StatusNotFound, "Cookie not found")
		return
	}

	info, err := helper.ParseClaims(value, h.cfg.SecretKey)

	if err != nil {
		c.AbortWithError(http.StatusForbidden, err)
		return
	}
	fmt.Println(info)

	h.DeleteCookieHandler(c)

}

func (h *Handler) DeleteCookieHandler(c *gin.Context) {
	c.SetCookie("token", "", -1, "/", "localhost", false, true)
	c.String(http.StatusOK, "Staff has been logout --> successfully")
}
