package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/content_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateDiller godoc
// @ID create_diller
// @Router /v1/diller [POST]
// @Summary  Create Diller
// @Description Create Diller
// @Tags Diller
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body content_service.CreateDiller true "CreateDillerRequestBody"
// @Success 200 {object} http.Response{data=content_service.Diller} "GetDillerBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateDiller(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var diller content_service.CreateDiller

	err := c.ShouldBindJSON(&diller)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.DillerService().Create(c, &diller)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetDillerByID godoc
// @ID get_diller_by_id
// @Router /v1/diller/{id} [GET]
// @Summary Get Diller By ID
// @Description Get Diller By ID
// @Tags Diller
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=content_service.Diller} "DillerBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDillerById(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	contentId := c.Param("id")

	resp, err := h.services.DillerService().GetByID(c, &content_service.DillerPrimaryKey{Id: contentId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetDillerList godoc
// @ID get_diller_list
// @Router /v1/diller [GET]
// @Summary Get Diller List
// @Description  Get Diller List
// @Tags Diller
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchFirstName query string false "searchFirstName"
// @Param searchLastName query string false "searchLastName"
// @Param searchPhone query string false "searchPhone"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=content_service.GetListDillerResponse} "GetAllDillerResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetDillerList(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.DillerService().GetList(
		context.Background(),
		&content_service.GetListDillerRequest{
			Limit:       int64(limit),
			Offset:      int64(offset),
			FirstName:   c.Query("searchFirstName"),
			LastName:    c.Query("searchLastName"),
			PhoneNumber: c.Query("searchPhone"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateDiller godoc
// @ID update_diller
// @Router /v1/diller/{id} [PUT]
// @Summary Update Diller
// @Description Update Diller
// @Tags Diller
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body content_service.UpdateDiller true "UpdateDillerRequestBody"
// @Success 200 {object} http.Response{data=content_service.Diller} "Diller data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateDiller(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var content content_service.UpdateDiller

	content.Id = c.Param("id")

	err := c.ShouldBindJSON(&content)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.DillerService().Update(
		c.Request.Context(),
		&content,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteDiller godoc
// @ID delete_Diller
// @Router /v1/diller/{id} [DELETE]
// @Summary Delete Diller
// @Description Delete Diller
// @Tags Diller
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Diller data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteDiller(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	contentId := c.Param("id")

	resp, err := h.services.DillerService().Delete(
		c.Request.Context(),
		&content_service.DillerPrimaryKey{Id: contentId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
