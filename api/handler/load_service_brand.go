package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/load_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateBrand godoc
// @ID create_brand
// @Router /v1/brand [POST]
// @Summary  Create Brand
// @Description Create Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body load_service.CreateBrand true "CreateBrandRequestBody"
// @Success 200 {object} http.Response{data=load_service.Brand} "GetBrandBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateBrand(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var brand load_service.CreateBrand

	err := c.ShouldBindJSON(&brand)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.BrandService().Create(c, &brand)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetBrandByID godoc
// @ID get_brand_by_id
// @Router /v1/brand/{id} [GET]
// @Summary Get Brand By ID
// @Description Get Brand By ID
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=load_service.Brand} "BrandBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBrandById(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	loadId := c.Param("id")

	resp, err := h.services.BrandService().GetByID(c, &load_service.BrandPrimaryKey{Id: loadId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetBrandList godoc
// @ID get_brand_list
// @Router /v1/brand [GET]
// @Summary Get Brand List
// @Description  Get Brand List
// @Tags Brand
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param searchName query string false "searchName"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=load_service.GetListBrandResponse} "GetAllBrandResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetBrandList(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.BrandService().GetList(
		context.Background(),
		&load_service.GetListBrandRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
			Name:   c.Query("searchName"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateBrand godoc
// @ID update_brand
// @Router /v1/brand/{id} [PUT]
// @Summary Update Brand
// @Description Update Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body load_service.UpdateBrand true "UpdateBrandRequestBody"
// @Success 200 {object} http.Response{data=load_service.Brand} "Brand data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateBrand(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var load load_service.UpdateBrand

	load.Id = c.Param("id")

	err := c.ShouldBindJSON(&load)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.BrandService().Update(
		c.Request.Context(),
		&load,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteBrand godoc
// @ID delete_Brand
// @Router /v1/brand/{id} [DELETE]
// @Summary Delete Brand
// @Description Delete Brand
// @Tags Brand
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Brand data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteBrand(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	loadId := c.Param("id")

	resp, err := h.services.BrandService().Delete(
		c.Request.Context(),
		&load_service.BrandPrimaryKey{Id: loadId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
