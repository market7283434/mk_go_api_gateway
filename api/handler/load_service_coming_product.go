package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/load_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateComingProducts godoc
// @ID create_coming_product
// @Router /v1/coming_product [POST]
// @Summary  Create ComingProducts
// @Description Create ComingProducts
// @Tags ComingProducts
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body load_service.CreateComingProducts true "CreateComingProductsRequestBody"
// @Success 200 {object} http.Response{data=load_service.ComingProducts} "GetComingProductsBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateComingProducts(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var coming_product load_service.CreateComingProducts

	err := c.ShouldBindJSON(&coming_product)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	comingData, err:= h.services.ComingService().GetList(c, &load_service.GetListComingRequest{
		ComingId: coming_product.ComingId,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	coming_product.ComingId=comingData.Comings[0].Id
	resp, err := h.services.ComingProductsService().Create(c, &coming_product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetComingProductsByID godoc
// @ID get_coming_product_by_id
// @Router /v1/coming_product/{id} [GET]
// @Summary Get ComingProducts By ID
// @Description Get ComingProducts By ID
// @Tags ComingProducts
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=load_service.ComingProducts} "ComingProductsBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingProductsById(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	loadId := c.Param("id")

	resp, err := h.services.ComingProductsService().GetByID(c, &load_service.ComingProductsPrimaryKey{Id: loadId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetComingProductsList godoc
// @ID get_coming_product_list
// @Router /v1/coming_product [GET]
// @Summary Get ComingProducts List
// @Description  Get ComingProducts List
// @Tags ComingProducts
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param brand_id query string false "brand_id"
// @Param coming_id query string false "coming_id"
// @Param category_id query string false "category_id"
// @Param barcode query string false "barcode"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=load_service.GetListComingProductsResponse} "GetAllComingProductsResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetComingProductsList(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ComingProductsService().GetList(
		context.Background(),
		&load_service.GetListComingProductsRequest{
			Limit:      int64(limit),
			Offset:     int64(offset),
			BrandId:    c.Query("brand_id"),
			CategoryId: c.Query("category_id"),
			ComingId:   c.Query("coming_id"),
			Barcode:    c.Query("barcode"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateComingProducts godoc
// @ID update_coming_product
// @Router /v1/coming_product/{id} [PUT]
// @Summary Update ComingProducts
// @Description Update ComingProducts
// @Tags ComingProducts
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body load_service.UpdateComingProducts true "UpdateComingProductsRequestBody"
// @Success 200 {object} http.Response{data=load_service.ComingProducts} "ComingProducts data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateComingProducts(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var load load_service.UpdateComingProducts

	load.Id = c.Param("id")

	err := c.ShouldBindJSON(&load)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ComingProductsService().Update(
		c.Request.Context(),
		&load,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteComingProducts godoc
// @ID delete_ComingProducts
// @Router /v1/coming_product/{id} [DELETE]
// @Summary Delete ComingProducts
// @Description Delete ComingProducts
// @Tags ComingProducts
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ComingProducts data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteComingProducts(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	loadId := c.Param("id")

	resp, err := h.services.ComingProductsService().Delete(
		c.Request.Context(),
		&load_service.ComingProductsPrimaryKey{Id: loadId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
