package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/load_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// Send Coming godoc
// @ID send_coming
// @Router /v1/do-income/{coming_id} [GET]
// @Summary Send Coming
// @Description Send Coming
// @Tags Send Coming
// @Accept json
// @Procedure json
// @Param coming_id path string true "coming_id"
// @Success 200 {object} http.Response{data=load_service.Remainder} "RemainderBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) Send_Coming(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusForbidden, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	var (
		coming_id = c.Param("coming_id")
	)

	coming, err := h.services.ComingService().GetList(c, &load_service.GetListComingRequest{
		ComingId: coming_id,
	})
	check:=coming.Comings[0]
	if check == nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Not Found This Coming",
			"message": err.Error(),
		})

		return
	}

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})

		return
	}
	if check.Status == "finished" {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "This Coming has already FINISHED!!!",
			"message": err.Error(),
		})

		return
	}

	// GET Coming Product

	product_data, err := h.services.ComingProductsService().GetList(c.Request.Context(), &load_service.GetListComingProductsRequest{
		ComingId: check.Id,
	})

	if product_data == nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "storage.comingProducts.Not_Found",
			"message": err.Error(),
		})
		return
	}
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "Get Product ---- Send Coming",
			"message": err.Error(),
		})
		return
	}

	fmt.Printf("%+v",check)

	for _, val := range product_data.ComingProducts {
		fmt.Printf("%+v", val)
		_, err := h.services.RemainderService().AddProduct(c, &load_service.Remainder{
			Barcode:    val.Barcode,
			BranchId:   check.BranchId,
			CategoryId: val.CategoryId,
			BrandId:    val.BrandId,
			Name:       val.Name,
			Price:      val.Price,
			Amount:     val.Amount,
		})

		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "Add Product to Remainder",
				"message": err.Error(),
			})
			return
		}
	}
	_, err = h.services.ComingService().Update(c, &load_service.UpdateComing{
		Id:       check.Id,
		ComingId: check.ComingId,
		BranchId: check.BranchId,
		DillerId: check.DillerId,
		Date:     time.Now().Format("2006-01-02"),
		Status:   "finished",
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"status":  "succes",
		"message": "Product Succesfully Added to Remainder",
	})

}
