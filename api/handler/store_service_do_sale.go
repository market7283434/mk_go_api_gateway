package handlers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/load_service"
	"gitlab.com/market/mk_go_api_gateway/genproto/store_service"
)

// @Security ApiKeyAuth
// ScanBarcode godoc
// @ID scan_barcode
// @Router /v1/scan_barcode [GET]
// @Summary  Scan Barcode
// @Description Scan Barcode
// @Tags Do Sale
// @Accept json
// @Produce json
// @Param barcode query string true "barcode"
// @Param sale_id query string true "sale_id"
// @Success 200 {object} http.Response{data=store_service.SaleProduct} "GetSaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) ScanBarcode(c *gin.Context) {
	var (
		barcode           = c.Query("barcode")
		saleID            = c.Query("sale_id")
		createSaleProduct = &store_service.CreateSaleProduct{}
		updateSaleProduct = &store_service.UpdateSaleProduct{}
	)

	// Get Sale Data

	saleData, err := h.services.SaleService().GetList(c, &store_service.GetListSaleRequest{
		SaleId: saleID,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	sale := saleData.Sales[0]

	branchID := sale.BranchId

	// //  Get Remain Product
	remainData, err := h.services.RemainderService().GetList(c, &load_service.GetListRemainderRequest{
		Barcode:  barcode,
		BranchId: branchID,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	if remainData.Count < 1 {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Remain Product",
			"message": "We have not enough Product",
		})
		return

	}
	fmt.Println("\n\n")

	fmt.Printf("%+v", remainData)

	fmt.Println("\n\n")
	// check if exsists

	check, err := h.services.SaleProductService().GetList(c, &store_service.GetListSaleProductRequest{
		SaleId:  sale.Id,
		Barcode: barcode,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	if check.Count > 0 {
		checkSale := check.SaleProducts[0]

		updateSaleProduct.Id = checkSale.Id
		updateSaleProduct.SaleId = sale.Id
		updateSaleProduct.BrandId = checkSale.BrandId
		updateSaleProduct.CategoryId = checkSale.CategoryId
		updateSaleProduct.Name = checkSale.Name
		updateSaleProduct.Barcode = barcode
		updateSaleProduct.Amount = checkSale.Amount + 1
		updateSaleProduct.RemainAmount = remainData.Remainders[0].Amount
		updateSaleProduct.Price = checkSale.Price

		resp, err := h.services.SaleProductService().Update(c, updateSaleProduct)

		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "GRPC ERROR update",
				"message": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, resp)

	} else {

		fmt.Println("else")

		// Datas

		createSaleProduct.SaleId = sale.Id
		createSaleProduct.BrandId = remainData.Remainders[0].BrandId
		createSaleProduct.CategoryId = remainData.Remainders[0].CategoryId
		createSaleProduct.Name = remainData.Remainders[0].Name
		createSaleProduct.Barcode = barcode
		createSaleProduct.Amount = 1
		createSaleProduct.RemainAmount = remainData.Remainders[0].Amount
		createSaleProduct.Price = remainData.Remainders[0].Price

		// Create Sale Product

		id, err := h.services.SaleProductService().Create(c, createSaleProduct)
		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "GRPC ERROR",
				"message": err.Error(),
			})
			return
		}

		resp, err := h.services.SaleProductService().GetByID(c, &store_service.SaleProductPrimaryKey{Id: id.Id})
		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "GRPC ERROR",
				"message": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, resp)
	}
}

// @Security ApiKeyAuth
// DoSaleProduct godoc
// @ID do_sale_product
// @Router /v1/do_sale_product [POST]
// @Summary  Do Sale Product
// @Description Do Sale Product
// @Tags Do Sale
// @Accept json
// @Produce json
// @Param profile body store_service.CreateSaleProduct true "CreateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=store_service.SaleProduct} "GetSaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DoSaleProduct(c *gin.Context) {

	var (
		sale_product store_service.CreateSaleProduct

		updateSaleProduct = &store_service.UpdateSaleProduct{}
	)

	err := c.ShouldBindJSON(&sale_product)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	saleData, err := h.services.SaleService().GetList(c, &store_service.GetListSaleRequest{
		SaleId: sale_product.SaleId,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	if saleData.Count < 1 {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Sale",
			"message": "We have not This Sale",
		})
		return
	}

	sale := saleData.Sales[0]
	// check if exsists

	check, err := h.services.SaleProductService().GetList(c, &store_service.GetListSaleProductRequest{
		SaleId:  sale.Id,
		Barcode: sale_product.Barcode,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	if check.Count > 0 {
		updateSaleProduct.Id = check.SaleProducts[0].Id
		updateSaleProduct.SaleId = sale.Id
		updateSaleProduct.BrandId = check.SaleProducts[0].BrandId
		updateSaleProduct.CategoryId = check.SaleProducts[0].CategoryId
		updateSaleProduct.Name = check.SaleProducts[0].Name
		updateSaleProduct.Barcode = sale_product.Barcode
		updateSaleProduct.Amount = sale_product.Amount
		updateSaleProduct.RemainAmount = check.SaleProducts[0].RemainAmount
		updateSaleProduct.Price = check.SaleProducts[0].Price

		resp, err := h.services.SaleProductService().Update(c, updateSaleProduct)

		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "GRPC ERROR",
				"message": err.Error(),
			})
			return
		}

		c.JSON(http.StatusOK, resp)

	} else {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Sale Product",
			"message": "This Sale Does Not Exist",
		})
	}
}

// @Security ApiKeyAuth
// DoSale godoc
// @ID do_sale
// @Router /v1/do_sale/{sale_id} [GET]
// @Summary  DoSale
// @Description DoSale
// @Tags Do Sale
// @Accept json
// @Produce json
// @Param sale_id path string true "sale_id"
// @Success 200 {object} http.Response{data=store_service.SaleProduct} "GetSaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DoSale(c *gin.Context) {
	var (
		sale_id = c.Param("sale_id")
	)

	saleData, err := h.services.SaleService().GetList(c, &store_service.GetListSaleRequest{
		SaleId: sale_id,
	})

	
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR get sales",
			"message": err.Error(),
		})
		return
	}
	
	if saleData.Count < 1 {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Sale",
			"message": "We have not This Sale",
		})
		return
	}
	
	sale := saleData.Sales[0]

	if sale.Status == "close" {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Sale",
			"message": "This Sale already closed",
		})
		return
	}

	
	salePd, err := h.services.SaleProductService().GetList(c, &store_service.GetListSaleProductRequest{
		SaleId: sale.Id,
	})
	fmt.Println()
	fmt.Printf("%+v", salePd)
	fmt.Println()
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR get sale Product",
			"message": err.Error(),
		})
		return
	}

	for _, val := range salePd.SaleProducts {
		resp, err:= h.services.RemainderService().DeleteProduct(c, &load_service.Remainder{
			BranchId: sale.BranchId,
			Barcode:  val.Barcode,
			Amount:   val.Amount,
		})

		if err != nil {
			c.JSON(http.StatusInternalServerError, map[string]interface{}{
				"status":  "GRPC ERROR sub product",
				"message": err.Error(),
			})
			return
		}
		fmt.Printf("%+v", resp)

	}

	resp, err := h.services.SaleService().Update(c, &store_service.UpdateSale{
		Id:       sale.Id,
		SaleId:   sale.SaleId,
		ShiftId:  sale.ShiftId,
		BranchId: sale.BranchId,
		StaffId:  sale.StaffId,
		MarketId: sale.MarketId,
		Status:   "close",
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "GRPC ERROR sale status",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)

}
