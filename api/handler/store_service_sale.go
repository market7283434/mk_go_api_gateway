package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/content_service"
	"gitlab.com/market/mk_go_api_gateway/genproto/store_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateSale godoc
// @ID create_sale
// @Router /v1/sale [POST]
// @Summary  Create Sale
// @Description Create Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body store_service.CreateSale true "CreateSaleRequestBody"
// @Success 200 {object} http.Response{data=store_service.Sale} "GetSaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSale(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var sale store_service.CreateSale
	err := c.ShouldBindJSON(&sale)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	check, err:= h.services.ShiftService().GetByID(c,&store_service.ShiftPrimaryKey{Id: sale.ShiftId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	if check==nil{
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Shift",
			"message": "You dont Have Open Shift!",
		})
	}

	count, err := h.services.SaleService().GetList(c, &store_service.GetListSaleRequest{})

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	branch, err := h.services.BranchService().GetByID(c, &content_service.BranchPrimaryKey{Id: sale.BranchId})

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	letter := branch.Name[0]
	SaleID := helper.GenerateString(string(letter), int(count.Count)+1)

	sale.SaleId = SaleID

	resp, err := h.services.SaleService().Create(c, &sale)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetSaleByID godoc
// @ID get_sale_by_id
// @Router /v1/sale/{id} [GET]
// @Summary Get Sale By ID
// @Description Get Sale By ID
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=store_service.Sale} "SaleBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleById(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.SaleService().GetByID(c, &store_service.SalePrimaryKey{Id: storeId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetSaleList godoc
// @ID get_sale_list
// @Router /v1/sale [GET]
// @Summary Get Sale List
// @Description  Get Sale List
// @Tags Sale
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param sale_id query string false "sale_id"
// @Param shift_id query string false "shift_id"
// @Param branch_id query string false "branch_id"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=store_service.GetListSaleResponse} "GetAllSaleResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleList(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SaleService().GetList(
		context.Background(),
		&store_service.GetListSaleRequest{
			Limit:    int64(limit),
			Offset:   int64(offset),
			SaleId:   c.Query("sale_id"),
			ShiftId:  c.Query("shift_id"),
			BranchId: c.Query("branch_id"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateSale godoc
// @ID update_sale
// @Router /v1/sale/{id} [PUT]
// @Summary Update Sale
// @Description Update Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body store_service.UpdateSale true "UpdateSaleRequestBody"
// @Success 200 {object} http.Response{data=store_service.Sale} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSale(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var store store_service.UpdateSale

	store.Id = c.Param("id")

	err := c.ShouldBindJSON(&store)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.SaleService().Update(
		c.Request.Context(),
		&store,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteSale godoc
// @ID delete_Sale
// @Router /v1/sale/{id} [DELETE]
// @Summary Delete Sale
// @Description Delete Sale
// @Tags Sale
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Sale data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSale(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.SaleService().Delete(
		c.Request.Context(),
		&store_service.SalePrimaryKey{Id: storeId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
