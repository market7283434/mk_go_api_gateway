package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/store_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateSaleProduct godoc
// @ID create_sale_product
// @Router /v1/sale_product [POST]
// @Summary  Create SaleProduct
// @Description Create SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body store_service.CreateSaleProduct true "CreateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=store_service.SaleProduct} "GetSaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateSaleProduct(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var sale_product store_service.CreateSaleProduct

	err := c.ShouldBindJSON(&sale_product)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SaleProductService().Create(c, &sale_product)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetSaleProductByID godoc
// @ID get_sale_product_by_id
// @Router /v1/sale_product/{id} [GET]
// @Summary Get SaleProduct By ID
// @Description Get SaleProduct By ID
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=store_service.SaleProduct} "SaleProductBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductById(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.SaleProductService().GetByID(c, &store_service.SaleProductPrimaryKey{Id: storeId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetSaleProductList godoc
// @ID get_sale_product_list
// @Router /v1/sale_product [GET]
// @Summary Get SaleProduct List
// @Description  Get SaleProduct List
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param brand_id query string false "brand_id"
// @Param sale_id query string false "sale_id"
// @Param category_id query string false "category_id"
// @Param barcode query string false "barcode"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=store_service.GetListSaleProductResponse} "GetAllSaleProductResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetSaleProductList(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.SaleProductService().GetList(
		context.Background(),
		&store_service.GetListSaleProductRequest{
			Limit:      int64(limit),
			Offset:     int64(offset),
			BrandId:    c.Query("brand_id"),
			CategoryId: c.Query("category_id"),
			SaleId:     c.Query("sale_id"),
			Barcode:    c.Query("barcode"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateSaleProduct godoc
// @ID update_sale_product
// @Router /v1/sale_product/{id} [PUT]
// @Summary Update SaleProduct
// @Description Update SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body store_service.UpdateSaleProduct true "UpdateSaleProductRequestBody"
// @Success 200 {object} http.Response{data=store_service.SaleProduct} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateSaleProduct(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var store store_service.UpdateSaleProduct

	store.Id = c.Param("id")

	err := c.ShouldBindJSON(&store)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.SaleProductService().Update(
		c.Request.Context(),
		&store,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteSaleProduct godoc
// @ID delete_SaleProduct
// @Router /v1/sale_product/{id} [DELETE]
// @Summary Delete SaleProduct
// @Description Delete SaleProduct
// @Tags SaleProduct
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "SaleProduct data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteSaleProduct(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.SaleProductService().Delete(
		c.Request.Context(),
		&store_service.SaleProductPrimaryKey{Id: storeId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
