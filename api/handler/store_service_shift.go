package handlers

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"gitlab.com/market/mk_go_api_gateway/genproto/store_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateShift godoc
// @ID create_shift
// @Router /v1/shift [POST]
// @Summary  Create Shift
// @Description Create Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body store_service.CreateShift true "CreateShiftRequestBody"
// @Success 200 {object} http.Response{data=store_service.Shift} "GetShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShift(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var shift store_service.CreateShift

	err := c.ShouldBindJSON(&shift)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	count, err := h.services.ShiftService().GetList(c, &store_service.GetListShiftRequest{})

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	ShiftID := helper.GenerateString("P", int(count.Count)+1)

	shift.ShiftId = ShiftID
	resp, err := h.services.ShiftService().Create(c, &shift)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetShiftByID godoc
// @ID get_shift_by_id
// @Router /v1/shift/{id} [GET]
// @Summary Get Shift By ID
// @Description Get Shift By ID
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=store_service.Shift} "ShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftById(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.ShiftService().GetByID(c, &store_service.ShiftPrimaryKey{Id: storeId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetShiftList godoc
// @ID get_shift_list
// @Router /v1/shift [GET]
// @Summary Get Shift List
// @Description  Get Shift List
// @Tags Shift
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=store_service.GetListShiftResponse} "GetAllShiftResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftList(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftService().GetList(
		context.Background(),
		&store_service.GetListShiftRequest{
			Limit:  int64(limit),
			Offset: int64(offset),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateShift godoc
// @ID update_shift
// @Router /v1/shift/{id} [PUT]
// @Summary Update Shift
// @Description Update Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body store_service.UpdateShift true "UpdateShiftRequestBody"
// @Success 200 {object} http.Response{data=store_service.Shift} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShift(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var store store_service.UpdateShift

	store.Id = c.Param("id")

	err := c.ShouldBindJSON(&store)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ShiftService().Update(
		c.Request.Context(),
		&store,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteShift godoc
// @ID delete_Shift
// @Router /v1/shift/{id} [DELETE]
// @Summary Delete Shift
// @Description Delete Shift
// @Tags Shift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "Shift data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShift(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.ShiftService().Delete(
		c.Request.Context(),
		&store_service.ShiftPrimaryKey{Id: storeId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// OpenShift godoc
// @ID open_shift
// @Router /v1/open_shift [POST]
// @Summary  Open Shift
// @Description Open Shift
// @Tags OPen Shift
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body store_service.CreateShift true "CreateShiftRequestBody"
// @Success 200 {object} http.Response{data=store_service.Shift} "GetShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) OpenShift(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var shift store_service.CreateShift

	err := c.ShouldBindJSON(&shift)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}

	check, err := h.services.ShiftService().GetList(c, &store_service.GetListShiftRequest{
		StaffId:  shift.StaffId,
		BranchId: shift.BranchId,
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	fmt.Println(check.Count)
	if check.Count > 0 {

		c.JSON(http.StatusNotAcceptable, map[string]interface{}{
			"status":  "Open",
			"message": "У этого филиала уже есть открытая смена. Пожалуйста, сначала закройте его",
		})
		return

	}
	count, err := h.services.ShiftService().GetList(c, &store_service.GetListShiftRequest{})

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	shiftID := helper.GenerateString("C", int(count.Count)+1)


	shift.ShiftId=shiftID
	resp, err := h.services.ShiftService().Create(c, &shift)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}

	fmt.Printf("%+v", resp)
	_, err = h.services.ShiftTransactionService().Create(c, &store_service.CreateShiftTransaction{
		ShiftId: resp.Id,
		Cash: 0,
		Uzcard: 0,
		Humo: 0,
		Payme: 0,
		Click: 0,
		Apelsin: 0,
		Visa: 0,
		ExchangeRate: 1,
		Currency: "UZS",
	})

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR shift transaction create",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// CloseShift godoc
// @ID close_shift
// @Router /v1/close_shift/{id} [GET]
// @Summary CloseShift
// @Description CloseShift
// @Tags CloseShift
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=store_service.Shift} "GetShiftBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CloseShift(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	shiftId := c.Param("id")
	shiftInfo, err := h.services.ShiftService().GetByID(c, &store_service.ShiftPrimaryKey{
		Id: shiftId,
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "close shift get by id",
			"message": err.Error(),
		})
		return
	}

	if shiftInfo.Status == "closed" {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "Closed",
			"message": "Касса успешно закрыта!!",
		})
		return
	}
	_, err = h.services.ShiftService().Update(c, &store_service.UpdateShift{
		Id:       cast.ToString(shiftId),
		BranchId: cast.ToString(shiftInfo.BranchId),
		ShiftId:  cast.ToString(shiftInfo.ShiftId),
		MarketId: cast.ToString(shiftInfo.MarketId),
		StaffId:  shiftInfo.StaffId,
		Status:   "closed",
	})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusBadRequest, map[string]interface{}{
		"status":  "Closed",
		"message": "Shift has been Successfully Closed!!",
	})
	return
}
