package handlers

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/market/mk_go_api_gateway/genproto/store_service"
	"gitlab.com/market/mk_go_api_gateway/pkg/helper"
)

// @Security ApiKeyAuth
// CreateShiftTransaction godoc
// @ID create_shift_transaction
// @Router /v1/shift_transaction [POST]
// @Summary  Create ShiftTransaction
// @Description Create ShiftTransaction
// @Tags ShiftTransaction
// @Accept json
// @Produce json
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Param profile body store_service.CreateShiftTransaction true "CreateShiftTransactionRequestBody"
// @Success 200 {object} http.Response{data=store_service.ShiftTransaction} "GetShiftTransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) CreateShiftTransaction(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var shift_transaction store_service.CreateShiftTransaction

	err := c.ShouldBindJSON(&shift_transaction)
	if err != nil {
		c.JSON(http.StatusInternalServerError, map[string]interface{}{
			"status":  "error",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftTransactionService().Create(c, &shift_transaction)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusCreated, resp)

}

// @Security ApiKeyAuth
// GetShiftTransactionByID godoc
// @ID get_shift_transaction_by_id
// @Router /v1/shift_transaction/{id} [GET]
// @Summary Get ShiftTransaction By ID
// @Description Get ShiftTransaction By ID
// @Tags ShiftTransaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=store_service.ShiftTransaction} "ShiftTransactionBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftTransactionById(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.ShiftTransactionService().GetByID(c, &store_service.ShiftTransactionPrimaryKey{Id: storeId})
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// GetShiftTransactionList godoc
// @ID get_shift_transaction_list
// @Router /v1/shift_transaction [GET]
// @Summary Get ShiftTransaction List
// @Description  Get ShiftTransaction List
// @Tags ShiftTransaction
// @Accept json
// @Produce json
// @Param offset query integer false "offset"
// @Param limit query integer false "limit"
// @Param shift_id query string false "shift_id"
// @Param Platform-Id header string true "Platform-Id" default(a1924766-a9ee-11ed-afa1-0242ac120001)
// @Success 200 {object} http.Response{data=store_service.GetListShiftTransactionResponse} "GetAllShiftTransactionResponseBody"
// @Response 400 {object} http.Response{data=string} "Invalid Argument"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) GetShiftTransactionList(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	offset, err := h.getOffsetParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	limit, err := h.getLimitParam(c)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	resp, err := h.services.ShiftTransactionService().GetList(
		context.Background(),
		&store_service.GetListShiftTransactionRequest{
			Limit:   int64(limit),
			Offset:  int64(offset),
			ShiftId: c.Query("shift_id"),
		},
	)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC ERROR",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)
}

// @Security ApiKeyAuth
// UpdateShiftTransaction godoc
// @ID update_shift_transaction
// @Router /v1/shift_transaction/{id} [PUT]
// @Summary Update ShiftTransaction
// @Description Update ShiftTransaction
// @Tags ShiftTransaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Param profile body store_service.UpdateShiftTransaction true "UpdateShiftTransactionRequestBody"
// @Success 200 {object} http.Response{data=store_service.ShiftTransaction} "ShiftTransaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) UpdateShiftTransaction(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}
	var store store_service.UpdateShiftTransaction

	store.Id = c.Param("id")

	err := c.ShouldBindJSON(&store)
	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "ShouldBindJSON",
			"message": err.Error(),
		})
		return
	}

	resp, err := h.services.ShiftTransactionService().Update(
		c.Request.Context(),
		&store,
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "BAD REQUEST",
			"message": err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, resp)

}

// @Security ApiKeyAuth
// DeleteShiftTransaction godoc
// @ID delete_ShiftTransaction
// @Router /v1/shift_transaction/{id} [DELETE]
// @Summary Delete ShiftTransaction
// @Description Delete ShiftTransaction
// @Tags ShiftTransaction
// @Accept json
// @Produce json
// @Param id path string true "id"
// @Success 200 {object} http.Response{data=object{}} "ShiftTransaction data"
// @Response 400 {object} http.Response{data=string} "Bad Request"
// @Failure 500 {object} http.Response{data=string} "Server Error"
func (h *Handler) DeleteShiftTransaction(c *gin.Context) {
	value, er := c.Cookie("token")
	if er != nil {
		c.String(http.StatusNotFound, " Cookie not found")
		return
	}

	_, er = helper.ParseClaims(value, h.cfg.SecretKey)

	if er != nil {
		c.AbortWithError(http.StatusForbidden, er)
		return
	}

	storeId := c.Param("id")

	resp, err := h.services.ShiftTransactionService().Delete(
		c.Request.Context(),
		&store_service.ShiftTransactionPrimaryKey{Id: storeId},
	)

	if err != nil {
		c.JSON(http.StatusBadRequest, map[string]interface{}{
			"status":  "GRPC",
			"message": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, resp)
}
