package services

import (
	"gitlab.com/market/mk_go_api_gateway/config"
	"gitlab.com/market/mk_go_api_gateway/genproto/content_service"
	"gitlab.com/market/mk_go_api_gateway/genproto/load_service"
	"gitlab.com/market/mk_go_api_gateway/genproto/store_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	BranchService() content_service.BranchServiceClient
	DillerService() content_service.DillerServiceClient
	StaffService() content_service.StaffServiceClient
	MarketService() content_service.MarketServiceClient

	ProductService() load_service.ProductServiceClient
	BrandService() load_service.BrandServiceClient
	CategoryService() load_service.CategoryServiceClient
	ComingService() load_service.ComingServiceClient
	ComingProductsService() load_service.ComingProductsServiceClient
	RemainderService() load_service.RemainderServiceClient

	SaleService() store_service.SaleServiceClient
	SaleProductService() store_service.SaleProductServiceClient
	PaymentService() store_service.PaymentServiceClient
	ShiftService() store_service.ShiftServiceClient
	ShiftTransactionService() store_service.ShiftTransactionServiceClient
}

type grpcClients struct {
	branch content_service.BranchServiceClient
	diller content_service.DillerServiceClient
	staff  content_service.StaffServiceClient
	market content_service.MarketServiceClient

	product        load_service.ProductServiceClient
	brand          load_service.BrandServiceClient
	category       load_service.CategoryServiceClient
	coming         load_service.ComingServiceClient
	comingProducts load_service.ComingProductsServiceClient
	remainder      load_service.RemainderServiceClient

	sale             store_service.SaleServiceClient
	saleProduct      store_service.SaleProductServiceClient
	payment          store_service.PaymentServiceClient
	shift            store_service.ShiftServiceClient
	shiftTransaction store_service.ShiftTransactionServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// connection to Content MicroService

	connContentService, err := grpc.Dial(
		cfg.CONTENTServiceHost+cfg.CONTENTGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	//  connect to Load MicroService
	connLoadService, err := grpc.Dial(
		cfg.LOADServiceHost+cfg.LOADGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	//  connect to Store MicroService
	connStoreService, err := grpc.Dial(
		cfg.STOREServiceHost+cfg.STOREGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)

	if err != nil {
		return nil, err
	}

	return &grpcClients{
		branch: content_service.NewBranchServiceClient(connContentService),
		diller: content_service.NewDillerServiceClient(connContentService),
		staff:  content_service.NewStaffServiceClient(connContentService),
		market: content_service.NewMarketServiceClient(connContentService),

		product:        load_service.NewProductServiceClient(connLoadService),
		brand:          load_service.NewBrandServiceClient(connLoadService),
		category:       load_service.NewCategoryServiceClient(connLoadService),
		coming:         load_service.NewComingServiceClient(connLoadService),
		comingProducts: load_service.NewComingProductsServiceClient(connLoadService),
		remainder:      load_service.NewRemainderServiceClient(connLoadService),

		sale: store_service.NewSaleServiceClient(connStoreService),
		saleProduct: store_service.NewSaleProductServiceClient(connStoreService),
		payment: store_service.NewPaymentServiceClient(connStoreService),
		shift: store_service.NewShiftServiceClient(connStoreService),
		shiftTransaction: store_service.NewShiftTransactionServiceClient(connStoreService),
	}, nil
}

func (g *grpcClients) BranchService() content_service.BranchServiceClient {
	return g.branch
}

func (g *grpcClients) DillerService() content_service.DillerServiceClient {
	return g.diller
}
func (g *grpcClients) StaffService() content_service.StaffServiceClient {
	return g.staff
}
func (g *grpcClients) MarketService() content_service.MarketServiceClient {
	return g.market
}

func (g *grpcClients) ProductService() load_service.ProductServiceClient {
	return g.product
}

func (g *grpcClients) BrandService() load_service.BrandServiceClient {
	return g.brand
}

func (g *grpcClients) CategoryService() load_service.CategoryServiceClient {
	return g.category
}

func (g *grpcClients) ComingService() load_service.ComingServiceClient {
	return g.coming
}

func (g *grpcClients) ComingProductsService() load_service.ComingProductsServiceClient {
	return g.comingProducts
}

func (g *grpcClients) RemainderService() load_service.RemainderServiceClient {
	return g.remainder
}
func (g *grpcClients) SaleService() store_service.SaleServiceClient {
	return g.sale
}

func (g *grpcClients) SaleProductService() store_service.SaleProductServiceClient {
	return g.saleProduct
}

func (g *grpcClients) PaymentService() store_service.PaymentServiceClient {
	return g.payment
}

func (g *grpcClients) ShiftService() store_service.ShiftServiceClient {
	return g.shift
}

func (g *grpcClients) ShiftTransactionService() store_service.ShiftTransactionServiceClient {
	return g.shiftTransaction
}

